from video_dataset import VideoFrameDataset, ImglistToTensor
from custom_transforms import Normalization
from torchvision.transforms import v2
import torch
from torch import optim
from torch.utils.data import DataLoader, random_split
from scipy.stats import pearsonr
from sklearn.metrics import mean_absolute_error, mean_squared_error
import os
from PhysFormer import ViViTBackbone
from PhysFormerLoss import Neg_Pearson, TorchLossComputer, estimate_hr_from_psd
import numpy as np
import math
from torchvision.transforms import InterpolationMode

def evaluate_model(dataloader, model, loss_fn, device, gra_sharp, epoch):
    model.eval()
    total_loss = 0
    all_outputs = []
    all_labels = []

    estimated_hr = []
    actual_hr = []

    a = 0.1
    b_start = 1.0
    exp_b = 5.0

    total_rPPG_loss = 0
    total_fre_loss = 0
    total_kl_loss = 0
    total_eval_mae = 0

    with torch.no_grad():
        for videos, labels in dataloader:
            videos = videos.to(device)
            fps, hr, bvp = labels
            hr = hr - 47
            fps = fps.to(device).float()
            hr = hr.to(device).float()
            bvp = bvp.to(device).float()

            outputs, _ = model(videos, gra_sharp)
            outputs = (outputs-torch.mean(outputs)) / torch.std(outputs)
            rPPG_loss = loss_fn(outputs, bvp)

            fre_loss = 0.0
            kl_loss = 0.0
            eval_mae = 0.0
            batch_size = videos.size(0)

            for bb in range(batch_size):
                loss_distribution_kl, fre_loss_temp, eval_mae_temp = TorchLossComputer.cross_entropy_power_spectrum_DLDL_softmax2(outputs[bb], hr[bb], fps[bb], std=1.0)  # std=1.1
                fre_loss = fre_loss + fre_loss_temp
                kl_loss = kl_loss + loss_distribution_kl
                eval_mae = eval_mae + eval_mae_temp

                hr_estimate = estimate_hr_from_psd(outputs[bb].cpu().numpy(), fps[bb].item(), len(outputs[bb]))
                estimated_hr.append(hr_estimate - 47)
                actual_hr.append(hr[bb].item())

            fre_loss = fre_loss/batch_size
            kl_loss = kl_loss/batch_size
            eval_mae = eval_mae/batch_size

            if epoch > 25:
                b = 5.0
            else:
                b = b_start*math.pow(exp_b, epoch/25.0)

            combined_loss = a*rPPG_loss + b*(fre_loss+kl_loss)

            total_loss += combined_loss.item()
            total_rPPG_loss += rPPG_loss.item()
            total_fre_loss += fre_loss.item()
            total_kl_loss += kl_loss.item()
            total_eval_mae += eval_mae.item()
            all_outputs.append(outputs.detach().cpu().numpy())
            all_labels.append(bvp.detach().cpu().numpy())

    avg_loss = total_loss / len(dataloader)
    avg_rPPG_loss = total_rPPG_loss / len(dataloader)
    avg_fre_loss = total_fre_loss / len(dataloader)
    avg_kl_loss = total_kl_loss / len(dataloader)
    avg_eval_mae = total_eval_mae / len(dataloader)

    estimated_hr = np.array(estimated_hr)
    actual_hr = np.array(actual_hr)
    metrics = calculate_metrics(np.array(estimated_hr), np.array(actual_hr), 'HR')
    r = np.corrcoef(estimated_hr, actual_hr)[0, 1]
    metrics.update({'HR Pearsons r': r})

    all_outputs = np.concatenate(all_outputs)
    all_labels = np.concatenate(all_labels)

    return metrics, avg_loss, avg_rPPG_loss, avg_fre_loss, avg_kl_loss, avg_eval_mae, all_outputs, all_labels

def calculate_metrics(outputs, labels, signal):
    sd = np.std(outputs - labels)
    mae = mean_absolute_error(labels, outputs)  
    mse = mean_squared_error(labels, outputs)
    rmse = np.sqrt(mean_squared_error(labels, outputs))
    #r, _ = pearsonr(outputs.flatten(), labels.flatten())
    return {f"{signal} SD": sd, 
            f"{signal} MAE": mae, 
            f"{signal} MSE": mse, 
            f"{signal} RMSE": rmse
            #f"{signal} Pearsons r": r
            }


def save_metrics(metrics, path, epoch):
    os.makedirs(path, exist_ok=True)
    epoch_str = str(epoch) if isinstance(epoch, int) else epoch
    for metric_name, metric_value in metrics.items():
        metric_file_path = os.path.join(path, f"{metric_name}.txt")
        with open(metric_file_path, 'a') as file:
            file.write(f'Epoch [{epoch_str}]: {metric_value:.4f}\n')


if __name__ == '__main__':
    #HEAVILY INSPIRED BY https://github.com/ZitongYu/PhysFormer/blob/main/train_Physformer_160_VIPL.py

    #seed_value= 42 

    #np.random.seed(seed_value)
    #torch.manual_seed(seed_value)
    #torch.cuda.manual_seed(seed_value)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    root = '/dataset/train'
    videos_root = os.path.join(os.getcwd(), root)
    annotations = os.path.join(root, 'annotations_no_hr_above_190.txt')

    val_set = '/dataset/val'
    videos_val = os.path.join(os.getcwd(), val_set)
    val_annotations = os.path.join(val_set, 'validation_annotations.txt')

    #test_path = '/dataset/test'
    #videos_test = os.path.join(os.getcwd(), test_path)
    #test_annotations = os.path.join(test_path, 'annotations.txt')

    preprocess = v2.Compose([
        v2.RandomHorizontalFlip(),
        #v2.RandomVerticalFlip(), 
        v2.RandomResizedCrop(128, scale=(0.85, 1.0), interpolation=InterpolationMode.BICUBIC),
        #v2.RandomRotation(degrees=(0, 30)),
        #v2.ElasticTransform(alpha=150),
        #v2.RandomPosterize(bits=2),
        #v2.RandomPerspective(distortion_scale=0.3, p=1.0),
        ImglistToTensor(),  # list of PIL images to (FRAMES x CHANNELS x HEIGHT x WIDTH) tensor
        Normalization(),
    ])

    val_test_preprocess = v2.Compose([
        ImglistToTensor(),
        Normalization(),
        ])

    frames_per_video = 160 # 300?
    height, width = 128, 128 # 224
    tubelet_size = 4 # 6
    patch_height, patch_width = 4, 4 # 8
    num_classes = 143 # 309-47
    heads = 4  #4 for physformer
    depth = 12 #12 for physformer
    batch_size = 4
    num_workers = 4
    lr = 0.0001
    epochs = 150
    gra_sharp = 2 #2 in physformer

    dataset = VideoFrameDataset( #from https://github.com/RaivoKoot/Video-Dataset-Loading-Pytorch/tree/main
            root=videos_root,
            annotationfile_path=annotations,
            num_segments=1,
            frames_per_segment=frames_per_video,
            imagefile_template='img_{:05d}.jpg',
            transform=preprocess,
            test_mode=False
        )
    
    val_dataset = VideoFrameDataset( 
            root=val_set,
            annotationfile_path=val_annotations,
            num_segments=1,
            frames_per_segment=frames_per_video,
            imagefile_template='img_{:05d}.jpg',
            transform=val_test_preprocess,
            test_mode=True
        )
    
    #test_dataset = VideoFrameDataset( 
    #        root=test_path,
    #        annotationfile_path=test_annotations,
    #        num_segments=1,
    #        frames_per_segment=frames_per_video,
    #        imagefile_template='img_{:05d}.jpg',
    #        transform=val_test_preprocess,
    #        test_mode=True
    #    )

    print(f"Size of training dataset: {len(dataset)}")
    print(f"Size of val dataset: {len(val_dataset)}")
    #print(f"Size of test dataset: {len(test_dataset)}")
    
    train_dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers, drop_last=True)
    val_dataloader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False, num_workers=num_workers, drop_last=True)
    #test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=num_workers, drop_last=True)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f'Device: {device}')

    #model = ViViTBackbone(t=frames_per_video, h=height, w=width, patch_t=tubelet_size, patch_h=patch_height, patch_w=patch_width, 
    #                     num_classes=num_classes, dim=96, depth=depth, heads=heads, mlp_dim=144, device='cuda').to(device) 
                                                                                    #frames, height, width, tubelet size, patch height, patch width, number of classes,
                                                                                    #dimension, depth, heads, mlp dimensions
                                                                                    #same values as in https://arxiv.org/pdf/2111.12082.pdf
    
    model = ViViTBackbone(image_size=(160,128,128), patches=(4,4,4), dim=96, ff_dim=144, num_heads=4, num_layers=12, dropout_rate=0.1, theta=0.7).to(device)

    
    #MAE = torch.nn.L1Loss() #MAE
    #poisson = torch.nn.PoissonNLLLoss(log_input=False) #Poisson loss?
    neg_pearson_loss = Neg_Pearson() #Negative Pearson loss
    optimizer = optim.Adam(model.parameters(), lr=lr)

    #model_path = 'epoch_32_model.pth'

    #checkpoint = torch.load(model_path)
    #model.load_state_dict(checkpoint['model_state_dict'])
    #optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    #epoch_number = checkpoint['epoch']

    metrics_dir = "training_metrics"
    os.makedirs(metrics_dir, exist_ok=True)

    #find training session number
    session_dirs = []
    for d in os.listdir(metrics_dir):
        if os.path.isdir(os.path.join(metrics_dir, d)):
            session_dirs.append(d)

    if not session_dirs:
        new_session_num = 1
    else:
        max_num = 0
        for d in session_dirs:
            num = int(d)
            if num > max_num:
                max_num = num
        new_session_num = max_num + 1

    session_dir = os.path.join(metrics_dir, str(new_session_num))
    os.makedirs(session_dir, exist_ok=True)

    print(f'Starting training session {new_session_num}.')

    #create models dir
    #best_models_dir = os.path.join(session_dir, 'models/best')
    #os.makedirs(best_models_dir, exist_ok=True)
    rest_models_dir = os.path.join(session_dir, 'models/rest')
    os.makedirs(rest_models_dir, exist_ok=True)

    best_val_loss = float('inf') 
    a = 0.1
    b_start = 1.0
    exp_b = 5.0
    

    for epoch in range(epochs):
        model.train()  
        print(f"Epoch {epoch+1} training.")
        total_loss = 0
        total_rPPG_loss = 0
        total_fre_loss = 0
        total_kl_loss = 0
        total_train_mae = 0

        estimated_hrs = []
        actual_hrs = []

        #np.random.seed(epoch) #reproducibility
        #torch.manual_seed(epoch)
        #torch.cuda.manual_seed(epoch)

        all_outputs = []
        all_labels = []

        for videos, labels in train_dataloader:
            videos = videos.to(device)
            fps, hr, bvp = labels
            hr = hr - 47
            fps = fps.to(device).float()
            hr = hr.to(device).float()
            bvp = bvp.to(device).float()

            #videos, bvp = random_reverse(videos, bvp)

            optimizer.zero_grad()
            outputs, _ = model(videos, gra_sharp)          #            Output = [b, 180]

            #print(sp_attn[0].shape)
            #print(temp_attn[0].shape)

            outputs = (outputs-torch.mean(outputs)) / torch.std(outputs)
            rPPG_loss = neg_pearson_loss(outputs, bvp)

            fre_loss = 0.0
            kl_loss = 0.0
            train_mae = 0.0           
            for bb in range(batch_size):
                 loss_distribution_kl, fre_loss_temp, train_mae_temp = TorchLossComputer.cross_entropy_power_spectrum_DLDL_softmax2(outputs[bb], hr[bb], fps[bb], std=1.0)  # std=1.1
                 fre_loss = fre_loss + fre_loss_temp
                 kl_loss = kl_loss + loss_distribution_kl
                 train_mae = train_mae + train_mae_temp
                 estimated_hr = estimate_hr_from_psd(outputs[bb].detach().cpu().numpy(), fps[bb].item(), len(outputs[bb]))
                 estimated_hrs.append(estimated_hr - 47)
                 actual_hrs.append(hr[bb].item())

            fre_loss = fre_loss/batch_size
            kl_loss = kl_loss/batch_size
            train_mae = train_mae/batch_size

            if epoch > 25:
                b = 5.0
            else:
                b = b_start*math.pow(exp_b, epoch/25.0)

            combined_loss = a*rPPG_loss + b*(fre_loss+kl_loss)
        
            combined_loss.backward()
            optimizer.step()

            total_loss += combined_loss.item()
            total_rPPG_loss += rPPG_loss.item()
            total_fre_loss += fre_loss.item()
            total_kl_loss += kl_loss.item()
            total_train_mae += train_mae.item()

            all_outputs.append(outputs.detach().cpu().numpy())
            all_labels.append(bvp.detach().cpu().numpy())

        avg_loss = total_loss / len(train_dataloader)
        avg_rPPG_loss = total_rPPG_loss / len(train_dataloader)
        avg_fre_loss = total_fre_loss / len(train_dataloader)
        avg_kl_loss = total_kl_loss / len(train_dataloader)
        avg_train_mae = total_train_mae / len(train_dataloader)

        epoch_outputs = np.concatenate(all_outputs)
        epoch_labels = np.concatenate(all_labels)

        estimated_hrs = np.array(estimated_hrs)
        actual_hrs = np.array(actual_hrs)

        metrics = calculate_metrics(estimated_hrs, actual_hrs, 'HR')
        r = np.corrcoef(estimated_hrs, actual_hrs)[0, 1]
        metrics.update({'HR Pearsons r': r})
        metrics.update(calculate_metrics(epoch_outputs, epoch_labels, 'rPPG'))
        metrics.update({
            "rPPG Pearsons r": 1 - avg_rPPG_loss,
            "Loss": avg_loss,
            "rPPG Physformer_MAE": avg_train_mae,
            "rPPG Peak_avg": avg_fre_loss,
            "rPPG kl_avg": avg_kl_loss
        })

        print(f'Epoch [{epoch+1}/{epochs}], Training Metrics: {metrics}')
        print(f"Starting epoch {epoch+1} validation.")

        val_metrics, val_loss, val_rPPG_loss, val_fre_loss, val_kl_loss, val_eval_mae, val_outputs, val_labels = evaluate_model(val_dataloader, model, neg_pearson_loss, 
                                                                                                                                device, gra_sharp, epoch)
        val_metrics.update(calculate_metrics(val_outputs, val_labels, 'rPPG'))
        val_metrics.update({
            "rPPG Pearsons r": 1 - val_rPPG_loss,
            "Loss": val_loss,
            "rPPG Physformer_MAE": val_eval_mae,
            "rPPG Peak_avg": val_fre_loss,
            "rPPG kl_avg": val_kl_loss
        })

        print(f'Epoch [{epoch+1}/{epochs}], Validation Metrics: {val_metrics}')

        #save metrics
        train_metrics_path = os.path.join(session_dir, 'train')
        save_metrics(metrics, train_metrics_path, epoch) 

        val_metrics_path = os.path.join(session_dir, 'val')
        save_metrics(val_metrics, val_metrics_path, epoch) 

        #if val_loss < best_val_loss:
        #    print(f"Validation improved, saving model")
        #    best_val_loss = val_loss  
        #    best_model_path = os.path.join(best_models_dir, f'best_model_val_loss_epoch_{epoch+1}.pth')
        #    torch.save({
        #        'epoch': epoch+1,
        #        'model_state_dict': model.state_dict(),
        #        'optimizer_state_dict': optimizer.state_dict()
        #    }, best_model_path)
        #else:
        model_path = os.path.join(rest_models_dir, f'epoch_{epoch+1}_model.pth')
        torch.save({
            'epoch': epoch+1,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict()
        }, model_path)

    #print(f"Starting test run.")
    #Test the model
    #test_loss, test_outputs, test_labels = evaluate_model(test_dataloader, model, neg_pearson_loss, device, gra_sharp, epochs)
    #test_metrics = calculate_metrics(test_outputs, test_labels)
    #test_metrics['Loss'] = test_loss
    #print(f'Final Test Metrics: {test_metrics}')

    #test_metrics_path = os.path.join(session_dir, 'test')
    #save_metrics(test_metrics, test_metrics_path, 'Final')
    last_model_path = os.path.join(rest_models_dir, f'last_model_epoch_{epochs}.pth')
    torch.save({
        'epoch': epochs,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict()
    }, last_model_path)