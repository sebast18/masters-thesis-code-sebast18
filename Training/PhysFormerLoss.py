#FROM https://github.com/ZitongYu/PhysFormer/blob/main/TorchLossComputer.py

'''
  modifed based on the HR-CNN    
  https://github.com/radimspetlik/hr-cnn
'''

import math
import torch
from torch.autograd import Variable
import numpy as np
import torch.nn.functional as F
import pdb
import torch.nn as nn
from scipy.signal import firwin, filtfilt, welch


def bpfilter64(s2, fs, max_len=160): #recreation of filter functions from https://github.com/ZitongYu/PhysFormer/tree/main
    minfq = 0.8 * 2 / fs  
    maxfq = 3.2 * 2 / fs    
    fir1_len = min(round(len(s2) / 10), max_len)
    bpfilter = firwin(fir1_len, [minfq, maxfq], window='hamming', pass_zero=False) #bandpass filter + hamming window
    s2f = filtfilt(bpfilter, 1, s2)
    return s2f

def estimate_hr_from_psd(signal, framerate, length):

    filtered_signal = bpfilter64(signal, framerate, length)
    filtered_signal = (filtered_signal-np.mean(filtered_signal)) / np.std(filtered_signal)
    #Welch's methodto calculate PSD
    f, Pxx = welch(filtered_signal, framerate, nfft=2**13, nperseg=length)

    Frange = np.where((f > 0.7) & (f < 4))[0]  
    
    #find peak frequency within Frange range and convert to BPM
    idxG = np.argmax(Pxx[Frange])
    hr_estimate = f[Frange][idxG] * 60 
    
    return hr_estimate
    
def normal_sampling(mean, label_k, std):
    return math.exp(-(label_k-mean)**2/(2*std**2))/(math.sqrt(2*math.pi)*std)

def kl_loss(inputs, labels):
    criterion = nn.KLDivLoss(reduction='none')
    outputs = torch.log(inputs)
    loss = criterion(outputs, labels)
    #loss = loss.sum()/loss.shape[0]
    loss = loss.sum()
    return loss

class Neg_Pearson(nn.Module):    # Pearson range [-1, 1] so if < 0, abs|loss| ; if >0, 1- loss
    def __init__(self):
        super(Neg_Pearson,self).__init__()
        return
    def forward(self, preds, labels):       # all variable operation
        loss = 0
        for i in range(preds.shape[0]):
            sum_x = torch.sum(preds[i])                # x
            sum_y = torch.sum(labels[i])               # y
            sum_xy = torch.sum(preds[i]*labels[i])        # xy
            sum_x2 = torch.sum(torch.pow(preds[i],2))  # x^2
            sum_y2 = torch.sum(torch.pow(labels[i],2)) # y^2
            N = preds.shape[1]
            #print(preds.shape)
            pearson = (N*sum_xy - sum_x*sum_y)/(torch.sqrt((N*sum_x2 - torch.pow(sum_x,2))*(N*sum_y2 - torch.pow(sum_y,2))))
            
            loss += 1 - pearson
            
        loss = loss/preds.shape[0]
        return loss
    

class TorchLossComputer(object):
    @staticmethod
    def compute_complex_absolute_given_k(output, k, N):
        two_pi_n_over_N = Variable(2 * math.pi * torch.arange(0, N, dtype=torch.float), requires_grad=True) / N
        hanning = Variable(torch.from_numpy(np.hanning(N)).type(torch.FloatTensor), requires_grad=True).view(1, -1)

        k = k.type(torch.FloatTensor).cuda()
        two_pi_n_over_N = two_pi_n_over_N.cuda()
        hanning = hanning.cuda()
            
        output = output.view(1, -1) * hanning
        output = output.view(1, 1, -1).type(torch.cuda.FloatTensor)
        k = k.view(1, -1, 1)
        two_pi_n_over_N = two_pi_n_over_N.view(1, 1, -1)
        complex_absolute = torch.sum(output * torch.sin(k * two_pi_n_over_N), dim=-1) ** 2 \
                           + torch.sum(output * torch.cos(k * two_pi_n_over_N), dim=-1) ** 2

        return complex_absolute

    @staticmethod
    def complex_absolute(output, Fs, bpm_range=None):
        output = output.view(1, -1)

        N = output.size()[1]

        unit_per_hz = Fs / N
        feasible_bpm = bpm_range / 60.0
        k = feasible_bpm / unit_per_hz
        
        # only calculate feasible PSD range [0.8,3.2]Hz (CORRESPONDS TO BPM RANGE OF 48-190)
        complex_absolute = TorchLossComputer.compute_complex_absolute_given_k(output, k, N)

        return (1.0 / complex_absolute.sum()) * complex_absolute	# Analogous Softmax operator
    
    @staticmethod
    def cross_entropy_power_spectrum_DLDL_softmax2(inputs, target, Fs, std):
        target_distribution = [normal_sampling(int(target), i, std) for i in range(144)]
        target_distribution = [i if i > 1e-15 else 1e-15 for i in target_distribution]
        target_distribution = torch.Tensor(target_distribution).cuda()
        
        #pdb.set_trace()
        
        rank = torch.Tensor([i for i in range(144)]).cuda()
        
        inputs = inputs.view(1, -1)
        target = target.view(1, -1)
        
        bpm_range = torch.arange(47, 191, dtype=torch.float).cuda()

        complex_absolute = TorchLossComputer.complex_absolute(inputs, Fs, bpm_range)
        
        fre_distribution = F.softmax(complex_absolute.view(-1), dim=0)
        loss_distribution_kl = kl_loss(fre_distribution, target_distribution)
                
        whole_max_val, whole_max_idx = complex_absolute.view(-1).max(0)
        whole_max_idx = whole_max_idx.type(torch.float)

        CE = F.cross_entropy(complex_absolute, target.view((1)).type(torch.long))
      
        return loss_distribution_kl, CE,  torch.abs(target[0] - whole_max_idx)