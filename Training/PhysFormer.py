from torch import nn, einsum
import torch
from einops.layers.torch import Rearrange
from einops import rearrange, repeat
from torch import nn, einsum
import torch
from einops.layers.torch import Rearrange
from einops import rearrange, repeat
import math
from torch.nn import functional as F
import numpy as np
from typing import Optional

# FROM https://github.com/ZitongYu/PhysFormer/blob/main/model/Physformer.py

def as_tuple(x):
    return x if isinstance(x, tuple) else (x, x)
    
class CDC_T(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1,
                 padding=1, dilation=1, groups=1, bias=False, theta=0.7):

        super(CDC_T, self).__init__()
        self.conv = nn.Conv3d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding,
                              dilation=dilation, groups=groups, bias=bias)
        self.theta = theta

    def forward(self, x):
        out_normal = self.conv(x)

        if math.fabs(self.theta - 0.0) < 1e-8:
            return out_normal
        else:
            # pdb.set_trace()
            [C_out, C_in, t, kernel_size, kernel_size] = self.conv.weight.shape

            # only CD works on temporal kernel size>1
            if self.conv.weight.shape[2] > 1:
                kernel_diff = self.conv.weight[:, :, 0, :, :].sum(2).sum(2) + self.conv.weight[:, :, 2, :, :].sum(
                    2).sum(2)
                kernel_diff = kernel_diff[:, :, None, None, None]
                out_diff = F.conv3d(input=x, weight=kernel_diff, bias=self.conv.bias, stride=self.conv.stride,
                                    padding=0, dilation=self.conv.dilation, groups=self.conv.groups)
                return out_normal - self.theta * out_diff

            else:
                return out_normal
            

class Attention(nn.Module):
    def __init__(self, dim, heads=8, dim_head=64, dropout=0., theta=0.7):
        super().__init__()

        self.heads = heads

        self.attnprobs = nn.Softmax(dim=-1)
        self.dropout = nn.Dropout(dropout)

        self.proj_q = nn.Sequential(
            CDC_T(dim, dim, 3, stride=1, padding=1, groups=1, bias=False, theta=theta),  
            nn.BatchNorm3d(dim),
        )
        self.proj_k = nn.Sequential(
            CDC_T(dim, dim, 3, stride=1, padding=1, groups=1, bias=False, theta=theta),  
            nn.BatchNorm3d(dim),
        )
        self.proj_v = nn.Sequential(
            nn.Conv3d(dim, dim, 1, stride=1, padding=0, groups=1, bias=False),  
        )

    def forward(self, x, gra_sharp):
        #Input into the factorised self-attention shape:                                 [30, 784, 96]      (b=t, h*w, mlp_dim)    h = height, w = width, b = batches, t = time

        b, n, c, h = *x.shape, self.heads #                                              [b, 640, 96]       (b=h*w, t, mlp_dim)
        #print(x.shape)

        x = rearrange(x, 'b (t h w) c -> b c t h w', h=8, w=8) #                         [4, 96, 40, 4, 4]  (b, mlp_dim, t, h, w)
        #print(x.shape)


        #Project input x to queries (q), keys (k) and values (v)
        #qkv = self.to_qkv(x).chunk(3, dim=-1) #                                         [3]

        q = self.proj_q(x) #                                                             [4, 96, 40, 4, 4]  (b, mlp_dim, t, h, w)
        #print(q.shape)
        k = self.proj_k(x) #                                                             [4, 96, 40, 4, 4]  (b, mlp_dim, t, h, w)
        #print(k.shape)
        v = self.proj_v(x) #                                                             [4, 96, 40, 4, 4]  (b, mlp_dim, t, h, w)
        #print(v.shape)

        #Rearrange tensors
        q, k, v = map(lambda t: rearrange(t, 'b d t h w -> b (t h w) d'), (q, k, v))  #  [4, 640, 96]       (b, t*h*w, d)  
        #print(q.shape)
        #print(k.shape)
        #print(v.shape)
        
        q, k, v = map(lambda t: rearrange(t, 'b s (d H) -> b H s d', H=h), (q, k, v)) #  [4, 4, 640, 24] H = HEADS
        #print(q.shape)
        #print(k.shape)
        #print(v.shape)

        #Compute scaled dot-product attention scores between queries and keys
        dots = einsum('b H q w, b H k w -> b H q k', q, k) / gra_sharp #                 [4, 4, 640, 640]   (b, heads, t*h*w, t*h*w) q = queries, k = keys        

        #Apply softmax function to attention scores to get attention probabilities
        scores = self.attend(dots) #                                                       [4, 4, 640, 640]  (b, heads, t*h*w, t*h*w)
        #print(attn.shape)
        scores = self.dropout(scores)

        #attn = rearrange(scores, 'b H q (t h w) -> b H q t (h w)', h=8, w=8)

        #Use attention probabilities to created weighted sum of values, 
        #which is the final attention output for each head
        out = einsum('b H S s, b H s w -> b S H w', scores, v) #                           [4, 640, 4, 24]    (b, heads, t*h*w, d)

        #print(out.shape)


        #Concatenate output of all heads
        out = rearrange(out, 'b S H w -> b S (H w)') #                                   [4, 640, 96]       (b, t*h*w, d)

        attn = rearrange(out, 'b (t h w) d -> b d t (h w)', h=8, w=8)
        #print(out.shape)
        return out, attn


class FeedForward(nn.Module):
    def __init__(self, dim, mlp_dim):
        super().__init__()
        
        self.fc1 = nn.Sequential(
            nn.Conv3d(dim, mlp_dim, 1, stride=1, padding=0, bias=False),  
            nn.BatchNorm3d(mlp_dim),
            nn.ELU(),
        )
        
        self.STConv = nn.Sequential(
            nn.Conv3d(mlp_dim, mlp_dim, 3, stride=1, padding=1, groups=mlp_dim, bias=False),  
            nn.BatchNorm3d(mlp_dim),
            nn.ELU(),
        )
        
        self.fc2 = nn.Sequential(
            nn.Conv3d(mlp_dim, dim, 1, stride=1, padding=0, bias=False),  
            nn.BatchNorm3d(dim),
        )

    def forward(self, x):  
        [B, P, C]=x.shape #                                       [b, 640, 96]
        #print(x.shape)
        x = rearrange(x, 'b (t h w) d -> b d t h w', h=8, w=8) #  [b, 96, 40, 4, 4]
        #print(x.shape)
        x = self.fc1(x)		                                    # [b, 144, 40, 4, 4]
        #print(x.shape)
        x = self.STConv(x)		                                # [b, 144, 40, 4, 4]
        #print(x.shape)
        x = self.fc2(x)		                                    # [4, 96, 40, 4, 4]
        #print(x.shape)
        x = rearrange(x, 'b d t h w -> b (t h w) d')            # [4, 640, 96]
        #print(x.shape)
        return x

class TransformerEncoder(nn.Module):
    """Transformer Block"""
    def __init__(self, dim, num_heads, num_layers, mlp_dim, dropout, theta):
        super().__init__()
        self.num_layers = num_layers
        self.attn = Attention(dim, num_heads, dropout, theta)
        self.proj = nn.Linear(dim, dim)
        self.norm1 = nn.LayerNorm(dim, eps=1e-6)
        self.pwff = FeedForward(dim, mlp_dim)
        self.norm2 = nn.LayerNorm(dim, eps=1e-6)
        self.drop = nn.Dropout(dropout)

    def forward(self, x, gra_sharp):
        attn_scores_array = []
        #                                                       [4, 640, 96]
        for _ in range(self.num_layers):
            Atten, Score = self.attn(self.norm1(x), gra_sharp)
            #print(Atten.shape)
            attn_scores_array.append(Score)
            h = self.drop(self.proj(Atten))
            #print(h.shape)
            x = x + h
            #print(x.shape)
            h = self.drop(self.pwff(self.norm2(x)))
            #print(h.shape)
            x = x + h
            #print(x.shape)
        return x, attn_scores_array


class ViViT(nn.Module):
    """ Factorised self-attention backbone of ViViT """

    def __init__(self, 
        name: Optional[str] = None, 
        pretrained: bool = False, 
        patches: int = 16,
        dim: int = 768,
        ff_dim: int = 3072,
        num_heads: int = 12,
        num_layers: int = 12,
        attention_dropout_rate: float = 0.0,
        dropout_rate: float = 0.2,
        representation_size: Optional[int] = None,
        load_repr_layer: bool = False,
        classifier: str = 'token',
        positional_embedding: str = '1d',
        in_channels: int = 3, 
        frame: int = 160,
        theta: float = 0.2,
        image_size: Optional[int] = None,
        ):
        super().__init__()

        self.image_size = image_size  
        self.frame = frame  
        self.dim = dim              

        # Image and patch sizes
        t, h, w = as_tuple(image_size)  # tube sizes
        ft, fh, fw = as_tuple(patches)  # patch sizes, ft = 4 ==> 160/4=80
        gt, gh, gw = t//ft, h // fh, w // fw  # number of patches
        seq_len = gh * gw * gt

        # Patch embedding    [4x16x16]conv
        self.patch_embedding = nn.Conv3d(dim, dim, kernel_size=(ft, fh, fw), stride=(ft, fh, fw))
        
        # Transformer
        self.transformer = TransformerEncoder(num_layers=num_layers, dim=dim, num_heads=num_heads, 
                                       mlp_dim=ff_dim, dropout=dropout_rate, theta=theta)
        
        self.Stem0 = nn.Sequential(
            nn.Conv3d(3, dim//4, [1, 5, 5], stride=1, padding=[0,2,2]),
            nn.BatchNorm3d(dim//4),
            nn.ReLU(inplace=True),
            nn.MaxPool3d((1, 2, 2), stride=(1, 2, 2)),
        )
        
        self.Stem1 = nn.Sequential(
            nn.Conv3d(dim//4, dim//2, [3, 3, 3], stride=1, padding=1),
            nn.BatchNorm3d(dim//2),
            nn.ReLU(inplace=True),
            nn.MaxPool3d((1, 2, 2), stride=(1, 2, 2)),
        )
        self.Stem2 = nn.Sequential(
            nn.Conv3d(dim//2, dim, [3, 3, 3], stride=1, padding=1),
            nn.BatchNorm3d(dim),
            nn.ReLU(inplace=True),
            nn.MaxPool3d((1, 1, 1), stride=(1, 1, 1)), #nn.MaxPool3d((1, 2, 2), stride=(1, 2, 2)) for 4x4 model
        )
        
        #self.normLast = nn.LayerNorm(dim, eps=1e-6)
        
        
        self.upsample = nn.Sequential(
            nn.Upsample(scale_factor=(2,1,1)),
            nn.Conv3d(dim, dim, [3, 1, 1], stride=1, padding=(1,0,0)),   
            nn.BatchNorm3d(dim),
            nn.ELU(),
        )
        self.upsample2 = nn.Sequential(
            nn.Upsample(scale_factor=(2,1,1)),
            nn.Conv3d(dim, dim//2, [3, 1, 1], stride=1, padding=(1,0,0)),   
            nn.BatchNorm3d(dim//2),
            nn.ELU(),
        )
 
        self.ConvBlockLast = nn.Conv1d(dim//2, 1, 1,stride=1, padding=0)
        
        
    def forward(self, x, gra_sharp):
        # Input into the network itself:                                         [b, 160, 3, 128, 128] (b, T, C, H, W)
        #print(x.shape)
        """ x is a video: (b, T, C, H, W) """
        
        x = rearrange(x, 'b T C H W -> b C T H W') #                             [b, 3, 160, 128, 128] (b, C, T, H, W)
        #print(x.shape)

        x = self.Stem0(x) #                                                      [b, 24, 160, 128, 128] (b, C, T, H, W) 
        #print(x.shape)
        x = self.Stem1(x) #                                                      [b, 48, 160, 32, 32]   (b, C, T, H, W) 
        #print(x.shape)
        x = self.Stem2(x) #                                                      [b, 96, 160, 16, 16]   (b, C, T, H, W) 
        #print(x.shape)


        x = self.patch_embedding(x) #                                            [b, 96, 40, 4, 4]
        #print(x.shape)
        x = rearrange(x, 'b c t h w -> b (t h w) c') #                           [b, 640, 96]
        #print(x.shape)
        
        x, attn_x = self.transformer(x, gra_sharp) #                              [b, 640, 96]        (b, t*h*w, mlp_dim)
        #print(x.shape)
        #x, attn_x_2 = self.transformer2(x, gra_sharp) #                           [b, 640, 96]        (b, t*h*w, mlp_dim)
        #print(x.shape)
        #x, attn_x_3 = self.transformer3(x, gra_sharp) #                           [b, 640, 96]        (b, t*h*w, mlp_dim)
        #print(x.shape)

        #x = x.transpose(1, 2).view(b, self.dim, t//4, 4, 4)
        x = rearrange(x, 'b (t h w) d -> b d t h w', h=8, w=8) #                  [b, 96, 40, 4, 4]  (b, mlp_dim, t, h, w)
        #print(x.shape)

        x = self.upsample(x)		    #                                         [b, 96, 80, 4, 4]
        #print(x.shape)
        x = self.upsample2(x)		    #                                         [b, 48, 160, 4, 4]
        #print(x.shape)

        x = torch.mean(x,3)     #                                                 [b, 48, 160, 4]  
        #print(x.shape)
        x = torch.mean(x,3)     #                                                 [b, 48, 160]  
        #print(x.shape)
        rPPG = self.ConvBlockLast(x)    #                                         [b, 1, 160]
        #print(rPPG.shape)
                
        rPPG = rPPG.squeeze(1) #                                                  [b, 160]

        return rPPG, attn_x #            Returns tensor with rPPG signal prediction