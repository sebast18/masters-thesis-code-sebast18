import torch

class Normalization(object):
    def __call__(self, x):
        return (x*2)-1